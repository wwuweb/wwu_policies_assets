<?php
/**
 * @file
 * wwu_policies_assets.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wwu_policies_assets_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_approved_by'.
  $permissions['create field_approved_by'] = array(
    'name' => 'create field_approved_by',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_dates_in_review'.
  $permissions['create field_dates_in_review'] = array(
    'name' => 'create field_dates_in_review',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_division'.
  $permissions['create field_division'] = array(
    'name' => 'create field_division',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_effective_date'.
  $permissions['create field_effective_date'] = array(
    'name' => 'create field_effective_date',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_file'.
  $permissions['create field_file'] = array(
    'name' => 'create field_file',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_organizational_unit'.
  $permissions['create field_organizational_unit'] = array(
    'name' => 'create field_organizational_unit',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_policy'.
  $permissions['create field_policy'] = array(
    'name' => 'create field_policy',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_revised'.
  $permissions['create field_revised'] = array(
    'name' => 'create field_revised',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_subject'.
  $permissions['create field_subject'] = array(
    'name' => 'create field_subject',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create og_group_ref'.
  $permissions['create og_group_ref'] = array(
    'name' => 'create og_group_ref',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_approved_by'.
  $permissions['edit field_approved_by'] = array(
    'name' => 'edit field_approved_by',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_dates_in_review'.
  $permissions['edit field_dates_in_review'] = array(
    'name' => 'edit field_dates_in_review',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_division'.
  $permissions['edit field_division'] = array(
    'name' => 'edit field_division',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_effective_date'.
  $permissions['edit field_effective_date'] = array(
    'name' => 'edit field_effective_date',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_file'.
  $permissions['edit field_file'] = array(
    'name' => 'edit field_file',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_organizational_unit'.
  $permissions['edit field_organizational_unit'] = array(
    'name' => 'edit field_organizational_unit',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_policy'.
  $permissions['edit field_policy'] = array(
    'name' => 'edit field_policy',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_revised'.
  $permissions['edit field_revised'] = array(
    'name' => 'edit field_revised',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_subject'.
  $permissions['edit field_subject'] = array(
    'name' => 'edit field_subject',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit og_group_ref'.
  $permissions['edit og_group_ref'] = array(
    'name' => 'edit og_group_ref',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_approved_by'.
  $permissions['edit own field_approved_by'] = array(
    'name' => 'edit own field_approved_by',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_dates_in_review'.
  $permissions['edit own field_dates_in_review'] = array(
    'name' => 'edit own field_dates_in_review',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_division'.
  $permissions['edit own field_division'] = array(
    'name' => 'edit own field_division',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_effective_date'.
  $permissions['edit own field_effective_date'] = array(
    'name' => 'edit own field_effective_date',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_file'.
  $permissions['edit own field_file'] = array(
    'name' => 'edit own field_file',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_organizational_unit'.
  $permissions['edit own field_organizational_unit'] = array(
    'name' => 'edit own field_organizational_unit',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_policy'.
  $permissions['edit own field_policy'] = array(
    'name' => 'edit own field_policy',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_revised'.
  $permissions['edit own field_revised'] = array(
    'name' => 'edit own field_revised',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_subject'.
  $permissions['edit own field_subject'] = array(
    'name' => 'edit own field_subject',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own og_group_ref'.
  $permissions['edit own og_group_ref'] = array(
    'name' => 'edit own og_group_ref',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_approved_by'.
  $permissions['view field_approved_by'] = array(
    'name' => 'view field_approved_by',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_dates_in_review'.
  $permissions['view field_dates_in_review'] = array(
    'name' => 'view field_dates_in_review',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_division'.
  $permissions['view field_division'] = array(
    'name' => 'view field_division',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_effective_date'.
  $permissions['view field_effective_date'] = array(
    'name' => 'view field_effective_date',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_file'.
  $permissions['view field_file'] = array(
    'name' => 'view field_file',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_organizational_unit'.
  $permissions['view field_organizational_unit'] = array(
    'name' => 'view field_organizational_unit',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_policy'.
  $permissions['view field_policy'] = array(
    'name' => 'view field_policy',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_revised'.
  $permissions['view field_revised'] = array(
    'name' => 'view field_revised',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_subject'.
  $permissions['view field_subject'] = array(
    'name' => 'view field_subject',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view og_group_ref'.
  $permissions['view og_group_ref'] = array(
    'name' => 'view og_group_ref',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_approved_by'.
  $permissions['view own field_approved_by'] = array(
    'name' => 'view own field_approved_by',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_dates_in_review'.
  $permissions['view own field_dates_in_review'] = array(
    'name' => 'view own field_dates_in_review',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_division'.
  $permissions['view own field_division'] = array(
    'name' => 'view own field_division',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_effective_date'.
  $permissions['view own field_effective_date'] = array(
    'name' => 'view own field_effective_date',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_file'.
  $permissions['view own field_file'] = array(
    'name' => 'view own field_file',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_organizational_unit'.
  $permissions['view own field_organizational_unit'] = array(
    'name' => 'view own field_organizational_unit',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_policy'.
  $permissions['view own field_policy'] = array(
    'name' => 'view own field_policy',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_revised'.
  $permissions['view own field_revised'] = array(
    'name' => 'view own field_revised',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_subject'.
  $permissions['view own field_subject'] = array(
    'name' => 'view own field_subject',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own og_group_ref'.
  $permissions['view own og_group_ref'] = array(
    'name' => 'view own og_group_ref',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
