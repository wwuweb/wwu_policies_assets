<?php
/**
 * @file
 * wwu_policies_assets.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wwu_policies_assets_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wwu_policies_assets_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wwu_policies_assets_node_info() {
  $items = array(
    'authority' => array(
      'name' => t('Authority'),
      'base' => 'node_content',
      'description' => t('An <em>Authority</em> is referenced by policies.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'draft_policy' => array(
      'name' => t('Draft Policy'),
      'base' => 'node_content',
      'description' => t('Automatically created by the system to store and manage data about a <em>Policy</em> still in the drafting phase.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'form' => array(
      'name' => t('Form'),
      'base' => 'node_content',
      'description' => t('<em>Form</em> is a metadata container for university form PDF files.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'policy' => array(
      'name' => t('Policy'),
      'base' => 'node_content',
      'description' => t('The <em>Policy</em> content type stores data about a university policy.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'procedure' => array(
      'name' => t('Procedure'),
      'base' => 'node_content',
      'description' => t('<em>Procedure</em> is a metadata container for university procedure PDF files.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'reference' => array(
      'name' => t('Reference'),
      'base' => 'node_content',
      'description' => t('<em>References</em> are cited by the "See Also" field on policies when the reference is not another policy.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'standard' => array(
      'name' => t('Standard'),
      'base' => 'node_content',
      'description' => t('<em>Standard</em> is a metadata container for university standard PDF files.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'task' => array(
      'name' => t('Task'),
      'base' => 'node_content',
      'description' => t('<em>Task</em> is a metadata container for university task PDF files.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
