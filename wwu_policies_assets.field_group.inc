<?php
/**
 * @file
 * wwu_policies_assets.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wwu_policies_assets_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_organization|node|policy|form';
  $field_group->group_name = 'group_organization';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'policy';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Website Categorization',
    'weight' => '5',
    'children' => array(
      0 => 'field_division',
      1 => 'field_subject',
      2 => 'field_organizational_unit',
      3 => 'field_dates_in_review',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Website Categorization',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-organization field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_organization|node|policy|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_policy_pdf|node|policy|form';
  $field_group->group_name = 'group_policy_pdf';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'policy';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'PDF Upload',
    'weight' => '19',
    'children' => array(
      0 => 'field_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'PDF Upload',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => 'Upload a complete PDF in place of entering all policy data in the form above.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_policy_pdf|node|policy|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('PDF Upload');
  t('Website Categorization');

  return $field_groups;
}
