<?php
/**
 * @file
 * wwu_policies_assets.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function wwu_policies_assets_filter_default_formats() {
  $formats = array();

  // Exported format: Clean HTML.
  $formats['clean_html'] = array(
    'format' => 'clean_html',
    'name' => 'Clean HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_autop' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => 50,
        'status' => 1,
        'settings' => array(
          'local_paths' => 'policyreview.wwu.edu',
          'protocol_style' => 'full',
          'local_paths_exploded' => array(
            0 => array(
              'path' => 'policyreview.wwu.edu',
            ),
            1 => array(
              'path' => '/policy/',
            ),
            2 => array(
              'path' => '/policy/',
              'host' => 'localhost',
            ),
          ),
          'base_url_host' => 'localhost',
        ),
      ),
    ),
  );

  return $formats;
}
