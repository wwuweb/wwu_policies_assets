<?php
/**
 * @file
 * wwu_policies_assets.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wwu_policies_assets_taxonomy_default_vocabularies() {
  return array(
    'approvers' => array(
      'name' => 'Approvers',
      'machine_name' => 'approvers',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'organizational_unit' => array(
      'name' => 'Organizational Unit',
      'machine_name' => 'organizational_unit',
      'description' => '',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'policy_owners' => array(
      'name' => 'Policy Owners',
      'machine_name' => 'policy_owners',
      'description' => '',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'policy_subjects' => array(
      'name' => 'Policy Subjects',
      'machine_name' => 'policy_subjects',
      'description' => 'Primary policy subject matter.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'university_divisions' => array(
      'name' => 'University Divisions',
      'machine_name' => 'university_divisions',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
