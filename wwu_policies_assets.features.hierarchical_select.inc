<?php
/**
 * @file
 * wwu_policies_assets.features.hierarchical_select.inc
 */

/**
 * Implements hook_hierarchical_select_default_configs().
 */
function wwu_policies_assets_hierarchical_select_default_configs() {
$configs = array();
$config = array(
  'save_lineage' => '0',
  'enforce_deepest' => '0',
  'resizable' => '0',
  'level_labels' => array(
    'status' => 0,
    'labels' => array(
      '0' => '',
      '1' => '',
    ),
  ),
  'dropbox' => array(
    'status' => 0,
    'title' => 'All selections',
    'limit' => '0',
    'reset_hs' => '1',
    'sort' => 0,
  ),
  'editability' => array(
    'status' => 1,
    'item_types' => array(
      '0' => '',
      '1' => '',
    ),
    'allowed_levels' => array(
      '0' => 1,
      '1' => 1,
    ),
    'allow_new_levels' => 0,
    'max_levels' => '0',
  ),
  'entity_count' => array(
    'enabled' => 0,
    'require_entity' => 0,
    'settings' => array(
      'count_children' => 0,
      'entity_types' => array(
        'node' => array(
          'count_node' => array(
            'authority' => 0,
            'page' => 0,
            'form' => 0,
            'policy' => 0,
            'procedure' => 0,
            'reference' => 0,
            'task' => 0,
          ),
        ),
      ),
    ),
  ),
  'animation_delay' => 400,
  'special_items' => array(),
  'render_flat_select' => 0,
  'config_id' => 'taxonomy-field_owner',
);

$configs['taxonomy-field_owner'] = $config;
return $configs;
}
